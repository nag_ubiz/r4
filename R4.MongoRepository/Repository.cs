﻿using MongoDB.Bson;
using MongoDB.Driver;
using R4.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace R4.MongoRepository
{
    public class Repository<T> : IRepository<T>
        where T : EntityBase
    {
        IMongoDatabase db;        

        public Repository(string server = "localhost", int port = 27017, string dbname = "R4")
        {
            var client = new MongoClient(new MongoClientSettings
            {
                ConnectTimeout = TimeSpan.FromSeconds(10),
                ConnectionMode = ConnectionMode.Direct,
                Server = new MongoServerAddress(server, port),
                ApplicationName = System.Threading.Thread.CurrentThread.Name
            });

            db = client.GetDatabase(dbname);
        }

        public List<T> Read(Expression<Func<T, bool>> filter)
        {
            FindOptions findOptions = new FindOptions();
            var collection = db.GetCollection<T>(typeof(T).Name).Find(filter, findOptions);            
            return collection.ToList();
        }

        public bool Create(T entity)
        {
            entity.Id = ObjectId.GenerateNewId().ToString();
            db.GetCollection<T>(typeof(T).Name).InsertOne(entity);
            return true;
        }

        public bool Update(T entity)
        {
            db.GetCollection<T>(typeof(T).Name).ReplaceOne(filter => filter.Id == entity.Id, entity);
            return true;
        }

        public bool Delete(string id)
        {
            db.GetCollection<T>(typeof(T).Name).DeleteOne(filter => filter.Id == id);
            return true;
        }
    }
}
