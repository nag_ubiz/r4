﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace R4.MongoRepository
{
    public class EntityBase
    {
        [BsonId]
        public string Id { get; set; }
    }
}
