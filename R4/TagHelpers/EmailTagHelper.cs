﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace R4.TagHelpers
{
    [HtmlTargetElement("email", TagStructure = TagStructure.WithoutEndTag)]
    public class EmailTagHelper : TagHelper
    {
        public string MailTo { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";    // Replaces <email> with <a> tag
            output.Attributes.SetAttribute("href", "mailto:" + MailTo);
            output.Content.SetContent(MailTo);
            output.TagMode = TagMode.StartTagAndEndTag;

        }
    }
}
