﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace R4.TagHelpers
{
    [HtmlTargetElement(Attributes = "underline")]
    public class UnderlineTagHelper : TagHelper
    {
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.RemoveAll("underline");
            output.PreContent.SetHtmlContent("<p style=\"text-decoration: underline\">");
            output.PostContent.SetHtmlContent("</p>");
        }
    }
}
