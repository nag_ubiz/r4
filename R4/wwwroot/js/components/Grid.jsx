﻿//import "babel-polyfill";

class Grid extends React.Component {
    renderGridColumns() {
        var gridColumns = [];
        for (var c in this.props.columns) {
            var col = this.props.columns[c];
            gridColumns.push(<th key={col} >{col}</th>);
        }
        return gridColumns;
    }

    renderRows() {
        var rows = [];
        for (var r in this.props.data) {
            var rowData = [];
            for (var c in this.props.data[r]) {
                rowData.push(<td key={c}>{this.props.data[r][c]}</td>);
            }
            rows.push(<tr key={this.props.data[r].Id}>{rowData}</tr>);
        }
        return rows;
    }

    /*renderActionButtons: function () {
        var buttons = [];

        return buttons;
    },*/

    render() {
        return (
            /*<div>
                {this.renderActionButtons()}
            </div>*/
            <table>
                <thead>
                    <tr>
                        {this.renderGridColumns()}
                    </tr>
                </thead>
                <tbody>
                    {this.renderRows()}
                </tbody>
            </table>
        );
    }
}

