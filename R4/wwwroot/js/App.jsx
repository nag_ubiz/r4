﻿class App {
    constructor() {
        this.settings = {};        
    }    

    getSettings() {
        return new Promise(resolve => {
            $.getJSON("/appsettings.json", null, function (appSettings) {
                resolve(appSettings);
            });
        });
    }

    async start(type) {

        application.settings = await application.getSettings();

        var url = application.settings.serviceUrl + urlSeperator + type.name;
        var dataSource = new DataSource(type.name, url);
        var data = await dataSource.load();
        
        var columns = type.properties;

        ReactDOM.render(
            <Grid type={type.name} data={data} columns={columns} />,
            document.getElementById('main-content')
        );
    }
}

const urlSeperator = "/";