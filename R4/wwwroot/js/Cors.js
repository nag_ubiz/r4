﻿var getXHR = function () {
    try { return new XMLHttpRequest(); } catch (e) { }
    try { return new ActiveXObject("Msxml2.XMLHTTP.6.0"); } catch (e) { }
    try { return new ActiveXObject("Msxml2.XMLHTTP.3.0"); } catch (e) { }
    try { return new ActiveXObject("Microsoft.XMLHttp"); } catch (e) { }
    console.err("Could not find XMLHttpRequest");
};

var ajaxCors = function (uri, method, data, success) {
    //make the actual XMLHttpRequest
    var xhr = getXHR();
    if ('withCredentials' in xhr) {
        xhr.withCredentials = true;
        console.log("Using XMLHttpRequest2 to make AJAX requests");
    }
    xhr.open(method, uri);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200 || xhr.status === 304) {
                var response = JSON.parse(xhr.responseText);
                if (response.status === "ok")
                {
                    if (success)
                        success(JSON.parse(response.responseText));
                }
                else if (response.status === "error") console.log("There was a problem with your request: " + response.message);
                else console.log("Response has been recieved using " + response.status);
            }
        }
        else console.log("Response recieved with status " + xhr.status);
    };
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
    //supported in new browsers...do JSONP based stuff in older browsers...figure out how
    xhr.setRequestHeader("Access-Control-Allow-Origin", "*");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.send(JSON.stringify(data));
};