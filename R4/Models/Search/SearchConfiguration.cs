﻿using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace R4.Models.Search
{
    public class SearchConfiguration
    {
        [BsonId]
        public string Id { get; set; }

        public string DataType { get; set; }

        public List<Action> Actions { get; set; }

        public List<ColumnInfo> Columns { get; set; }
        
    }
}
