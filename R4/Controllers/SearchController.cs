﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using R4.Models;
using R4.Models.Search;

namespace R4.Controllers
{
    public class SearchController : Controller
    {
        Repository<SearchConfiguration> repo;

        public SearchController()
        {
            repo = new Repository<SearchConfiguration>("localhost", 27017, "NEP");
        }

        [Route("Search/")]
        public IActionResult Get(string dataType)
        {
            var config = repo.Get(filter => filter.DataType == dataType).FirstOrDefault() ?? new SearchConfiguration
            {
                Actions = new List<Models.Search.Action>(),
                Columns = new List<ColumnInfo>(),
                DataType = dataType
            };

            if (string.IsNullOrEmpty(config.Id))
                repo.Put(config);
                     
            return Json(repo);
        }
    }
}