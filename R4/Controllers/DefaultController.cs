﻿using Microsoft.AspNetCore.Mvc;

namespace R4.Controllers
{
    public class DefaultController : Controller
    {
        public IActionResult Start()
        {
            return View();
        }        
    }
}