﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using R4.Models;

namespace R4.Controllers
{
    public class CustomerController : Controller
    {
        public IActionResult Index()
        {
            List<Customer> customers = new List<Customer>();
            customers.Add(new Customer
            {
                Name = "John Peter",
                Address = "NY, USA",
                Phone = "(10) 838 374 7777"
            });
            customers.Add(new Customer
            {
                Name = "Ela Fernandez",
                Address = "Mumbai, India",
                Phone = "(022) 4732 7772"
            });
            return Json(customers);
        }
    }
}