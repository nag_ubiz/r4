﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace R4.Repository
{
    public interface IRepository<T>
    {
        bool Create(T entity);
        List<T> Read(Expression<Func<T, bool>> filter);
        bool Update(T entity);
        bool Delete(string Id);  
    }
}
