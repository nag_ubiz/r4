﻿using Xunit;
using R4.MongoRepository;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using R4.Services.CRM.Controllers;
using R4.Services.CRM.Models;

namespace R4.UnitTests
{
    public class CustomerControllerTests
    {
        [Fact]        
        public void GetTests()
        {
            CustomerController controller = new CustomerController(new Repository<Customer>());

            var result = controller.Get() as JsonResult;
            dynamic gridData = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(result.Value));

            Assert.NotNull(gridData.data);
        }
    }
}
