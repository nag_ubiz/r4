﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using R4.Repository;
using R4.Services.CRM.Models;

namespace R4.Services.CRM.Controllers
{
    [EnableCors("AllowAllMethods")]
    [Route("api/[controller]")]
    public class CustomerController : Controller
    {
        IRepository<Customer> repo;

        public CustomerController(IRepository<Customer> repository)
        {
            repo = repository;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<Customer> Get()
        {
            return repo.Read(filter => true);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public Customer Get(string id)
        {
            return repo.Read(filter => filter.Id == id).FirstOrDefault();
        }

        // POST api/values
        [HttpPost]
        public bool Post([FromBody]Customer value)
        {
            repo.Create(value);
            return true;
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public bool Put(int id, [FromBody]Customer value)
        {
            repo.Update(value);
            return true;
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            repo.Delete(id);
        }
    }
}