﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using R4.Services.CRM.Models;

namespace R4.Services.CRM.Controllers
{
    [EnableCors("AllowAllMethods")]
    [Produces("application/json")]
    [Route("api/Authenticate")]
    public class AuthenticateController : Controller
    {
        // POST api/values
        [HttpPost]
        public LoginInfo Post([FromBody]LoginInfo value)
        {
            value.IsAuthenticated = true;
            return value;
        }
    }
}