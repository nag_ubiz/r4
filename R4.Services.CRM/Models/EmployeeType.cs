﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace R4.Services.CRM.Models
{
    public enum EmployeeType
    {
        Crew = 0,
        Employee = 1,
        Contract = 2,
    }
}
