﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace R4.Services.CRM.Models
{
    public class LoginInfo
    {
        public string UserId { get; set; }

        public string Password { get; set; }

        public bool IsAuthenticated { get; set; }
    }
}
