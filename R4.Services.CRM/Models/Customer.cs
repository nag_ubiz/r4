﻿using R4.MongoRepository;

namespace R4.Services.CRM.Models
{
    public class Customer : EntityBase
    {
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public EmployeeType Type { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public string UserId { get; set; }

        public string Password { get; set; }
    }
}
